const Event = use('Event');

// send verify code when register email
Event.on('email::register', 'User.registerEmail');
