'use strict';

const Controller = use('App/Controllers/Controller');
const UserService = use('App/Services/UserService');

class UserController extends Controller {
  constructor () {
    super();
    this.userService = new UserService();
  }

  /**
   * register email
   * @param request
   * @returns {Promise.<{status: number, data: *, message: *, error: number}>}
   */
  async postRegisterEmail ({ request }) {
    const { email } = request.only(['email']);
    this.userService.sendVerifyCode(email);
    return this.buildSuccess({});
  }

  /**
   * create account
   * @param request
   * @returns {Promise.<*>}
   */
  async postRegister ({ request }) {
    const params = request.all();
    const body = await this.userService.register(params);
    return body;
  }
}

module.exports = UserController;
