'use strict';

const Validate = use('App/Validators/Validate.js');

class Login extends Validate {
  get rules () {
    return {
      email: 'required|email',
      password: 'required|min:6',
    }
  }

  get messages () {
    return {
      'email.required': 'email_is_required',
      'email.email': 'email_is_invalid',
      'password.required': 'password_is_required',
      'password.min': 'password_is_too_short'
    }
  }
}

module.exports = Login;
