'use strict';

const Controller = use('App/Controllers/Controller');
const GroupService = use('App/Services/GroupService');

class GroupController extends Controller {
  constructor () {
    super();
    this.groupService = new GroupService();
  }

  async getListGroup () {
    const data = await this.groupService.getListGroup();
    return this.buildSuccess({ data });
  }
}

module.exports = GroupController;
