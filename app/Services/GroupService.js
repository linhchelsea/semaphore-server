'use strict';

const GroupRepository = use('App/Repositories/GroupRepository');

class GroupService {
  constructor () {
    this.groupRepo = new GroupRepository();
  }

  async getListGroup () {
    const list = await this.groupRepo.getListGroup();
    list.forEach((group) => {
      group.content = group.content.split(',');
    });
    return list;
  }
}

module.exports = GroupService;
