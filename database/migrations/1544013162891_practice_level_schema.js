'use strict';

const Schema = use('Schema');

class PracticeLevelSchema extends Schema {
  up () {
    this.create('practice_levels', (table) => {
      table.bigIncrements();
      table.integer('level');
      table.string('content');
      table.timestamps();
    })
  }

  down () {
    this.drop('practice_levels');
  }
}

module.exports = PracticeLevelSchema;
