'use strict';

const Controller = use('App/Controllers/Controller');
const UserService = use('App/Services/UserService');

class AuthController extends Controller {
  constructor () {
    super();
    this.userService = new UserService();
  }

  async postLogin ({ request, auth }) {
    const { email, password } = request.only(['email', 'password']);
    const data = await this.userService.login(auth, email, password);
    return this.buildSuccess({ data });
  }

  async postLogout ({  auth }) {
    const userId = +auth.user.id;
    await this.userService.logout(userId);
    return this.buildSuccess({});
  }
}

module.exports = AuthController;
