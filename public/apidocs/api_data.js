define({ "api": [
  {
    "type": "GET",
    "url": "/practice?level=:level",
    "title": "Get question for level",
    "name": "Get_question_for_level",
    "group": "Practice",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "level",
            "description": "<p>Level practice.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": true,
            "field": "error",
            "defaultValue": "0",
            "description": "<p>error code</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "message",
            "defaultValue": "success",
            "description": "<p>response message</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>data response</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": true,
            "field": "status",
            "defaultValue": "200",
            "description": "<p>status response</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success response:",
          "content": "{\n   \"status\": 200,\n   \"data\": [\n     { \"text\": \"A\", \"time\": 3 },\n     { \"text\": \"B\", \"time\": 3 },\n     { \"text\": \"C\", \"time\": 3 },\n     { \"text\": \"D\", \"time\": 3 },\n     { \"text\": \"E\", \"time\": 3 },\n     { \"text\": \"F\", \"time\": 3 },\n     { \"text\": \"G\", \"time\": 3 },\n     { \"text\": \"H\", \"time\": 3 },\n     { \"text\": \"I\", \"time\": 3 },\n     { \"text\": \"J\", \"time\": 3 },\n   ],\n   \"message\": \"success\",\n   \"error\": 0\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/ApiDocs/Document.js",
    "groupTitle": "Practice"
  }
] });
