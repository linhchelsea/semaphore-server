'use strict';

const Group = use('App/Models/Group');

class GroupRepository {
  async getListGroup () {
    const list = await Group.all();
    return list.toJSON();
  }
}

module.exports = GroupRepository;
