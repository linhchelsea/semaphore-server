'use strict';

const Schema = use('Schema');

class UserVerifySchema extends Schema {
  up () {
    this.create('user_verifies', (table) => {
      table.bigIncrements();
      table.string('email').notNullable();
      table.string('code');
      table.boolean('status').default(false);
      table.timestamps();
    })
  }

  down () {
    this.drop('user_verifies');
  }
}

module.exports = UserVerifySchema;
