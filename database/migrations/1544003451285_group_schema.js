'use strict';

const Schema = use('Schema');

class GroupSchema extends Schema {
  up () {
    this.create('groups', (table) => {
      table.bigIncrements();
      table.string('name');
      table.enum('type', ['text', 'number']);
      table.string('content');
      table.timestamps();
    });
  }

  down () {
    this.dropIfExists('groups');
  }
}

module.exports = GroupSchema;
