'use strict';

const Controller = use('App/Controllers/Controller');
const PracticeService = use('App/Services/PracticeService');

class PracticeController extends Controller {
  constructor () {
    super();
    this.practiceService = new PracticeService();
  }
  async getListQuestion ({ request }) {
    const level = +request.input('level') || 1;
    const time = [1, 3, 5].indexOf(level) >= 0 ? 3 : 1;
    const data = await this.practiceService.getListQuestion(level, time);
    return this.buildSuccess({ data });
  }
}

module.exports = PracticeController;
