'use strict';

const { LogicalException } = require('@adonisjs/generic-exceptions');

class UserNotFoundException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
    return response.status(404)
      .send({
        status: 404,
        data: null,
        message: 'user_not_found',
        error: 0,
      });
  }
}

module.exports = UserNotFoundException;
