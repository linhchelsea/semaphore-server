'use strict';

const Env = use('Env');
const Mail = use('Mail');
class User {
  registerEmail ({ email, code }) {
    try {
      const from = Env.get('MAIL_USERNAME');
      Mail.send('verify', {code}, (message) => {
        message.subject('Verify Email')
          .from(from)
          .to(email);
      });
    } catch (e) {
      console.log(e);
    }
  }
}

module.exports = User;
