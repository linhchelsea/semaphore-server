'use strict';

const jwt = use('jsonwebtoken');
const util = use('util');
const verifyToken = util.promisify(jwt.verify);
const Env = use('Env');

const UserRepository = use('App/Repositories/UserRepository');

class JwtAuth {
  constructor () {
    this.userRepo = new UserRepository();
  }
  async handle ({ request, response, auth }, next) {
    let authToken = request.header('Authorization');
    authToken = authToken.split(' ');
    authToken = (authToken.length === 2 && authToken[0] === 'Bearer') ? authToken[1] : null;
    const jwtInfo = await verifyToken(authToken, Env.get('APP_KEY'));
    const user = await this.userRepo.getUserById(jwtInfo.uid);
    if (!user) {
      return response.status(401)
        .send({
          status: 401,
          data: null,
          message: 'invalid_token',
          error: 401,
        });
    }
    auth.user = user;
    await next()
  }
}

module.exports = JwtAuth;
