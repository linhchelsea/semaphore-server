'use strict';

const PracticeLevel = use('App/Models/PracticeLevel');
const data = ['xinchaocacban',
'daulonghaiatonga',
'thapmuoidepnhatbongsen',
'motconvitxoerahaicaicanh',
'iloveyou',
'howareyou',
'whatisyourname',
'vietnamvodich',
'cungnhaudotlua',
'toidihoc',
'cuocsongma',
'didaucungnhoquenhanhocanhraumuonnhocadamtuong',
'ahihidongok',
'occholacothat',
'keeptheblueflagflyinghigh',
'londonisblue',
'iloveyousomuch',
'thanhphodanangxinhdep',
'bacholalanhtuvidaicuadantocvietnam',
'namquocsonhanamdecutietnhiendinhphantaithienthu',
'coconchimvanhkhuyennhodangtrongthatduconqua',
'xinchaocacfanyeuquy',
'goodmorning',
'saysomething',
'barackobama',
'nguyenmanhlinh',
'songxaanhchangdedang',
'fastandfurious'
];
class CreatePracticeLevelOneSeeder {
  async run () {
    for (let i = 0; i < data.length; i += 1) {
      await PracticeLevel.create({
        level: 2,
        content: data[i],
      });
    }
  }
}

module.exports = CreatePracticeLevelOneSeeder;
