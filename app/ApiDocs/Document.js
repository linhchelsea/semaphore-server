/**
 * @api {GET} /practice?level=:level Get question for level
 * @apiName Get question for level
 * @apiGroup Practice
 *
 * @apiParam {Number} level Level practice.
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 *{
 *    "status": 200,
 *    "data": [
 *      { "text": ["A"], "time": 3 },
 *      { "text": ["B"], "time": 3 },
 *      { "text": ["C"], "time": 1 },
 *      { "text": ["D"], "time": 1 },
 *      { "text": ["E"], "time": 3 },
 *      { "text": ["F"], "time": 1 },
 *      { "text": ["G"], "time": 3 },
 *      { "text": ["H"], "time": 3 },
 *      { "text": ["I"], "time": 3 },
 *      { "text": ["J"], "time": 1 },
 *    ],
 *    "message": "success",
 *    "error": 0
 *}
 */
