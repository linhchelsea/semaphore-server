'use strict';

const { LogicalException } = require('@adonisjs/generic-exceptions');

class EmailOrPasswordNotCorrectException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
    return response.status(404)
      .send({
        status: 500,
        data: null,
        message: 'email_or_password_not_correct',
        error: 1,
      });
  }
}

module.exports = EmailOrPasswordNotCorrectException;
