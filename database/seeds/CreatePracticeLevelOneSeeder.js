'use strict';

const PracticeLevel = use('App/Models/PracticeLevel');
const data = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
class CreatePracticeLevelOneSeeder {
  async run () {
    for (let i = 0; i < data.length; i += 1) {
      await PracticeLevel.create({
        level: 0,
        content: data[i],
      });
    }
  }
}

module.exports = CreatePracticeLevelOneSeeder;
