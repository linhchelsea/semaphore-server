'use strict';

const Group = use('App/Models/Group');
const data = [
  { name: 'Group1: ABCDEFG', type: 'text', content: 'A,B,C,D,E,F,G' },
  { name: 'Group2: URN', type: 'text', content: 'U,R,N' },
  { name: 'Group 3: HIOW', type: 'text', content: 'H,I,O,W' },
  { name: 'Group 4: KLM', type: 'text', content: 'K,L,M' },
  { name: 'Group 5: PQST', type: 'text', content: 'P,Q,S,T' },
  { name: 'Group 6: JVXYZ', type: 'text', content: 'J,V,X,Y,Z' },
  { name: 'Group 7: Numbers', type: 'number', content: '0,1,2,3,4,5,6,7,8,9' },
];

class CreateGroupLearnSeeder {
  async run () {
    await Group.createMany(data);
  }
}

module.exports = CreateGroupLearnSeeder
