'use strict';

const Logger = use('Logger');

class Validate {
  async fails(errorMessages) {
    return this.ctx.response.status(400).send({
      status: 400,
      data: null,
      message: errorMessages[0].message,
      error: 400,
    });
  }

  async errorResponse (error) {
    return this.ctx.response.status(400).send({
      status: 400,
      data: null,
      message: error.message,
      error: error.error,
    });
  }

  authorizeFails(errorMessages) {
    Logger.error('( RobinSNS401 ): authorizeFails %j', errorMessages);
    return this.ctx.response.status(401).send({
      status: 401,
      data: null,
      message: errorMessages,
      error: 401,
    });
  }

  authorizeError(data) {
    return this.ctx.response.status(401).send({
      status: 401,
      data: null,
      message: data.message,
      error: data.error,
    });
  }

  resourceNotFound (data) {
    return this.ctx.response.status(404).send({
      status: 404,
      data: null,
      message: data.message,
      error: data.error,
    });
  }
}

module.exports = Validate;
