'use strict';

const Practice = use('App/Models/PracticeLevel');

class PracticeRepository {
  async getListQuestion (questionIds) {
    return Practice.query().whereIn('id', questionIds).fetch();
  }
}

module.exports = PracticeRepository;
