'use strict';

const User = use('App/Models/User');

class UserRepository {
  async createUser ({ email, password }) {
    await User.create({ email, password });
  }

  async getUserByEmail (email) {
    const user = await User.findBy({ email });
    return user ? user.toJSON() : null;
  }

  async getUserById (id) {
    const user = await User.find(id);
    return user ? user.toJSON() : null;
  }
}

module.exports = UserRepository;
