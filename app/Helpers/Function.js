'use strict';

module.exports = {
  randomString: (length) => {
    const string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let result = '';
    for (let i = 0; i < length; i += 1) {
      const index = Math.floor((Math.random() * string.length));
      result += string[index];
    }
    return result;
  }
};
