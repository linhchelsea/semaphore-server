'use strict';

const UserVerify = use('App/Models/UserVerify');

class UserVerifyRepository {
  async createNewRegister (email, code) {
    await UserVerify.findOrCreate({ email, code });
  }

  async getUserVerifyByEmailAndCode (email, code) {
    return UserVerify.query()
      .where('email', email)
      .where('code', code)
      .first();
  }

  async updateVerifyStatus (email, code) {
    await UserVerify.query()
      .where('email', email)
      .where('code', code)
      .update({ status: 1 });
    await UserVerify.query()
      .where('email', email)
      .where('status', 0)
      .delete();
  }
}

module.exports = UserVerifyRepository;
