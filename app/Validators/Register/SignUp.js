'use strict';

const Validate = use('App/Validators/Validate.js');

class SignUp extends Validate {
  get rules () {
    return {
      email: 'required|email',
      code: 'required|max:6|min:6',
      password: 'required|min:6',
      password_confirm: 'required|min:6',
    }
  }

  get messages () {
    return {
      'email.required': 'email_is_required',
      'email.email': 'email_is_invalid',
      'code.required': 'code_is_required',
      'code.max': 'code_is_invalid',
      'code.min': 'code_is_invalid',
      'password.required': 'password_is_required',
      'password.min': 'password_too_short',
      'password.max': 'password_too_long',
      'password_confirm.required': 'password_confirm_is_required',
      'password_confirm.min': 'password_confirm_too_short',
      'password_confirm.max': 'password_confirm_too_long',
    }
  }
}

module.exports = SignUp;
