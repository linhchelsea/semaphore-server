
const Route = use('Route');

const prefix = '/api';

Route.on('/').render('welcome');

Route.post('/login', 'AuthController.postLogin')
  .validator('Auth/Login');

Route.group(() => {
  Route.post('/sign-up', 'UserController.postRegister')
    .validator('Register/SignUp');

  Route.post('/register-email', 'UserController.postRegisterEmail')
    .validator('Register/RegisterEmail');

}).prefix(prefix);
Route.group(() => {
  Route.post('/logout', 'AuthController.postLogout')
  Route.get('/group/list', 'GroupController.getListGroup');
  Route.get('practices', 'PracticeController.getListQuestion');
})
  .prefix(prefix)
  .middleware(['jwtAuth']);

