'use strict';

const Token = use('App/Models/Token');

class TokenRepository {
  async updateToken (userId, jwt) {
    const token = await Token
      .query()
      .where('user_id', userId)
      .orderBy('id', 'desc')
      .first();
    token.jwt_token = jwt.token;
    await token.save();
    return token.toJSON();
  }

  async revokeToken (userId, tokenId) {
    await Token.query()
      .where('user_id', userId)
      .whereNot('id', tokenId)
      .update({
        is_revoked: 1,
        jwt_token: null,
      });
  }
}

module.exports = TokenRepository;
