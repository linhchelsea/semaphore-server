'use strict';

const Schema = use('Schema');

class TokensSchema extends Schema {
  up () {
    this.create('tokens', (table) => {
      table.bigIncrements();
      table.bigInteger('user_id').unsigned().references('id').inTable('users');
      table.string('token', 255).notNullable().unique().index();
      table.string('type', 80).notNullable();
      table.string('jwt_token');
      table.string('device_token');
      table.boolean('is_revoked').defaultTo(false);
      table.timestamps();
    })
  }

  down () {
    this.drop('tokens');
  }
}

module.exports = TokensSchema;
