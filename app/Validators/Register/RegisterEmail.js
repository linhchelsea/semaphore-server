'use strict';

const Validate = use('App/Validators/Validate.js');

class RegisterRegisterEmail extends Validate {
  get rules () {
    return {
      email: 'required|email',
    }
  }

  get messages () {
    return {
      'email.required': 'email_is_required',
      'email.email': 'email_is_invalid',
    }
  }
}

module.exports = RegisterRegisterEmail;
