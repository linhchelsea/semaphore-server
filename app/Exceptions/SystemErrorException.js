'use strict';

const { LogicalException } = require('@adonisjs/generic-exceptions');

class SystemErrorException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
    return response.status(404)
      .send({
        status: 500,
        data: null,
        message: 'something_went_wrong',
        error: 500,
      });
  }
}

module.exports = SystemErrorException;
