'use strict';

const UserRepository = use('App/Repositories/UserRepository');
const UserVerifyRepository = use('App/Repositories/UserVerifyRepository');
const TokenRepository = use('App/Repositories/TokenRepository');
const SystemErrorException = use('App/Exceptions/SystemErrorException');
const UserNotFoundException = use('App/Exceptions/UserNotFoundException');
const EmailOrPasswordNotCorrectException = use('App/Exceptions/EmailOrPasswordNotCorrectException');
const Event = use('Event');

const randomString = use('App/Helpers/Function').randomString;

class UserService {
  constructor () {
    this.userRepo = new UserRepository();
    this.userVerifyRepo = new UserVerifyRepository();
    this.tokenRepo = new TokenRepository();
  }

  async login (auth, email, password) {
    const user = await this.userRepo.getUserByEmail(email);
    if (!user) {
      throw new UserNotFoundException();
    }

    try {
      const jwt = await auth.withRefreshToken().attempt(email, password);
      const currentToken = await this.tokenRepo.updateToken(user.id, jwt);
      // revoked token
      await this.tokenRepo.revokeToken(user.id, currentToken.id);
      return jwt;
    } catch (e) {
      throw new EmailOrPasswordNotCorrectException();
    }
  }

  async logout (userId) {
    try {
      // revoked token
      await this.tokenRepo.revokeToken(userId, 0);
    } catch (e) {
      throw new SystemErrorException();
    }
  }

  /**
   * send verify code
   * @param email
   * @returns {Promise.<void>}
   */
  async sendVerifyCode (email) {
    const code = randomString(6);
    await this.userVerifyRepo.createNewRegister(email, code);
    Event.fire('email::register', { email, code });
  }

  async register (params) {
    const userVerify = await this.userVerifyRepo.getUserVerifyByEmailAndCode(params.email, params.code);
    if (!userVerify) {
      return {
        status: 404,
        data: null,
        message: 'verify_not_found',
        error: 0,
      }
    }

    if (userVerify.status) {
      return {
        status: 400,
        data: null,
        message: 'verified',
        error: 0,
      }
    }

    if (params.password !== params.password_confirm) {
      return {
        status: 400,
        data: null,
        message: 'password_confirm_miss_match',
        error: 0,
      }
    }

    await this.userVerifyRepo.updateVerifyStatus(params.email, params.code);
    await this.userRepo.createUser({
      email: params.email,
      password: params.password,
    });
    return {
      status: 200,
      data: null,
      message: 'success',
      error: 0,
    };
  }
}

module.exports = UserService;
